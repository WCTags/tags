**Infrequently updated/committed repo of my WCTAGS side project.<br>
Primarly for backups and pulling to linux host server for occasional cross-compatibilty checking.**

**Mainly using this project to learn and experiment with various ideas.**<br>
**Started June 2020**

**Current planned tech stack is:**

1)  Vanilla JS/Server side rendered pages set on half of front end.<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; -Meant to keep consistent with currently live UX.<br/>
        &nbsp;&nbsp;&nbsp;&nbsp; -Uses EJS Template/view engine.<br/>

    Complemented with React project for second half of website.<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;-Vanilla JS is too hard to update and maintain for that section.<br/>
        &nbsp;&nbsp;&nbsp;&nbsp;-React components work well with SQL bquery part of project. Cosmetically and functionally.

2)  NodeJS with Express backend for server. Written on windows with linux cross-compatibilty in mind.<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;Planning CentOS 8 host OS.<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;Transfering dev to Fedora in future is a goal. To write natively in RHEL LINUX family.<br/>

3)  Database is being tested with POSTGRESQL.<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;-A lighter alternative like SQLite will probably be used on live server.<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;-No need for a heavy DBMS on a light-SQL, single person project.<br/>

4)  Hosting is meant to be easily transferable.<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;-After feature  checking and pricing, AWS lightsail CentOS host is default choice.<br/>
    &nbsp;&nbsp;&nbsp;&nbsp;-Subject to change in need.<br/>

