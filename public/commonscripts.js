//returns a cookie expiration date
function getExpiration() {
	var expires = new Date();
	if(expires.getMonth() != 12){
		expires.setMonth(expires.getMonth() + 1);
	} else {
		expires.setMonth(1);
		expires.setYear(expires.getYear() + 1);
	}
	expires = '; expires=' + expires.toUTCString();
	return expires;
}

function setCookie(color,tagQty){
    let cookieStr = color + '=' + tagQty;
    cookieStr += getExpiration();
    document.cookie = cookieStr;
}
