
document.getElementById('subButton').addEventListener('click', buttonPress);

//returns a cookie expiration date
function getExpiration() {
	var expires = new Date();
	if(expires.getMonth() != 12){
		expires.setMonth(expires.getMonth() + 1);
	} else {
		expires.setMonth(1);
		expires.setYear(expires.getYear() + 1);
	}
	expires = '; expires=' + expires.toUTCString();
	return expires;
}
function setCookie(color,tagQty){
    let cookieStr = color + '=' + tagQty;
    cookieStr += getExpiration();
    document.cookie = cookieStr;
}

function buttonPress() {
    //return value from form
    let tagQty = document.getElementById('qty').value;
    console.log(tagQty);
    //check if number is valid
    //if valid-set cookies, else display error
    if(tagQty > 0 && tagQty < 30){
	let color = document.title.split(' ').shift();
    setCookie(color,tagQty)
    //change page on success
    document.getElementById('invalid-form').style.display = 'none';
    document.getElementById('checkout-link').style.display = 'inline-block'
    } else {
        //change page on failure
        document.getElementById('invalid-form').style.color = 'red';
    }
}
