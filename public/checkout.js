let price = 27;
var ttl = 0;
let colorValues = new Map();
colorValues.set('Red',0);
colorValues.set('Orange',0);
colorValues.set('Yellow',0);
colorValues.set('Green',0);
colorValues.set('Blue',0);
colorValues.set('Purple',0);
colorValues.set('White',0);

setValues();


total();
colorValues.forEach(setListeners);


function setValues(){
	let cookies = document.cookie.split("; ")
	cookies.forEach(element => {
		tempMap = element.split('=');
		colorValues.set(tempMap[0],parseInt(tempMap[1]));
	})
}

function total() {
	ttl = 0;
	colorValues.forEach(colorUpdate)
	document.getElementById("grandTotal").innerHTML = "$" + ttl;
}

function colorUpdate(value,key,map){
document.getElementById(key + "Value").innerHTML = value;
document.getElementById(key + 'Total').innerHTML = '$' + (value*price);
ttl += value*price;
}


function setListeners(value,key,map){
document.getElementById(key + 'Minus').addEventListener("click", function() {
	updateValue(key, "subtract")});
document.getElementById(key + 'Add').addEventListener("click", function() {
	updateValue(key, "add")});
document.getElementById(key + 'Clear').addEventListener("click", function() {
	updateValue(key, 'zero')})
}

function updateValue(color,sign){
	if(sign == 'subtract' && colorValues.get(color) != 0){
		colorValues.set(color, colorValues.get(color) - 1)
	} else if(sign == 'add' && colorValues.get(color) != 30) {
		colorValues.set(color, colorValues.get(color) + 1)
	} else if(sign == 'zero') {
		colorValues.set(color, 0);
	}
	setCookie(color, colorValues.get(color))
	total();
}


