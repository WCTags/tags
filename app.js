const { rejects } = require('assert');
//install express and basic utilities
const express = require('express');
const path = require('path');
const app = express();
const port = 3000;
const price = '$27.00';
console.log(port,price);

//view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
console.log("Engine done")

//setup public folder
console.log(path.join(__dirname,"public"));
app.use(express.static("public"));
console.log("Static done")

//start server
const server = app.listen(port, () => {
    console.log('Listening on port ' + port)
})
console.log("Server on")

//get home page
app.get('/', function(req,res) {
    res.render('pages/html-home.ejs')
});

//get shop page
app.get('/shop', function(req,res) {
    res.render('pages/html-shop.ejs', {"itemPrice":price})
});

//get a single tag color page
app.get('/:color', function(req,res,next) {
    let cols = ["Red-Tag","Orange-Tag","Yellow-Tag","Green-Tag","Blue-Tag","Purple-Tag","White-Tag"]
    if(cols.includes(req.params.color)){
        let color = req.params.color.split('-').shift();
        console.log(color);
        res.render(`pages/html-item.ejs`, {"itemColor":color,"itemPrice":price})
    } else {
        next()
    }
});

//get cart page
app.get('/cart',function (req,res) {
    res.render('pages/html-cart.ejs')
})

//get price
app.get('/price', (req, res) => {
    res.send(price);
})

//database setup
const { Client } = require('pg');
const { allowedNodeEnvironmentFlags } = require('process');

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'maindb',
    password: '1122',
    port: 5432,
})

client.connect(err => {
    if(err){
        console.log('database connection error');
    } else {
        console.log('database connected');
    }
})

zipQuery(98116).then((res) => console.log(res))

async function zipQuery(zipcode){
    let zipCust;
    await client.query("SELECT * FROM customers WHERE zipcode=" + zipcode)
        .then(((res) => zipCust = res.rows))
     //   .then(() => client.end())
    return zipCust;
}

app.get('/zip-search/:zip', (req, res) => {
    zipQuery(98116).then((resq) => res.send(resq));
})



